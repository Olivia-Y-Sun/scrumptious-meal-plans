from django import forms
from meal_plans.models import MealPlan
from recipes.models import Recipe

class MealPlanForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "author",
            "description",
            "image",
        ]


from meal_plans.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
