from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from recipes.models import Recipe

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
    USER_MODEL,
    related_name="user",
    on_delete=models.CASCADE,
    null=True,
    )
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    recipes = models.ManyToManyField("recipes.Recipe", related_name="meal_plans")

    def __str__(self):
        return self.name + " by " + str(self.owner)        # without the str, report error

# class Rating(models.Model):
#     value = models.PositiveSmallIntegerField(
#         validators=[
#             MaxValueValidator(5),
#             MinValueValidator(1),
#         ]
#     )
#     meal_plan = models.ForeignKey(
#         "MealPlan",
#         related_name="ratings",
#         on_delete=models.CASCADE,
#     )

#     def __str__(self):
#         return f"{self.meal_plan.name} rating is: {self.value}"
    