
from django.contrib import admin

from meal_plans.models import MealPlan 
# from meal_plans.models import Rating


# Register your models here.
class MealPlanAdmin(admin.ModelAdmin):
    pass


# class RatingAdmin(admin.ModelAdmin):
#     pass


admin.site.register(MealPlan, MealPlanAdmin)
# admin.site.register(Rating, RatingAdmin)

