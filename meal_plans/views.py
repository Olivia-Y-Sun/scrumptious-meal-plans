from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from meal_plans.models import MealPlan
# from meal_plans.models import Rating
# from meal_plans.forms import RatingForm

# Create your views here.

class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 10
    context_object_name = "meal_plan_list"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("LOOK, I AM HERE!!!!!!:", context)
        return context

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    # This is the golden key to make your webpage work!
    context_object_name = "meal_plan"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("LOOK, I AM HERE!!!!!!:", context)
        return context

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["rating_form"] = RatingForm()
    #     return context

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    # fields = ["name", "author", "description", "image"]
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("meal_plan_new")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("LOOK, I AM HERE!!!!!!:", context)
        return context

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan_detail", pk=plan.id)

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    # fields = ["name", "author", "description", "image"]
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("meal_plan_edit")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("LOOK, I AM HERE!!!!!!:", context)
        return context

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_delete")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print("LOOK, I AM HERE!!!!!!:", context)
        return context

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


        

