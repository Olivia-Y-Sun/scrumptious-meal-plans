from django.db import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse_lazy
# Add these for login, one for class, one for funtion
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.decorators.http import require_http_methods
from recipes.admin import IngredientAdmin

from recipes.forms import RatingForm
from recipes.forms import ServingForm

from recipes.forms import RecipeForm
from recipes.models import Ingredient, Recipe, ShoppingItem

# Add login decorator for function
@login_required
def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


@login_required
def serving_form(request, recipe_id):
    if request.method == "POST":
        form = ServingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 10
    # Add this one to every view
    context_object_name = "recipes_list"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context


# class RecipeDetailView(DetailView):
#     model = Recipe
#     template_name = "recipes/detail.html"
#     paginate_by = 10

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["rating_form"] = RatingForm()
#         return context


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = RatingForm()

        foods = []
        for item in self.request.user.shopping_items.all():
            foods.append(item.food_item)

        context["servings"] = self.request.GET.get("servings")

        context["food_in_shopping_list"] = foods
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    # fields = ["name", "author", "description", "image"]
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    # fields = ["name", "author", "description", "image"]
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")



class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"
    paginate_by = 10
    context_object_name = "shoppingitem_list"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


def delete_all_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    context_object_name = "shoppingitem_list"
    return redirect("shopping_item_list")

# class create_shopping_item(LoginRequiredMixin, CreateView):
#     pass

@require_http_methods(["POST"])
def create_shopping_item(request):
    # Get the ingredient_id from the POST
    ingredient_id = request.POST.get("ingredient_id")
    # Get the specific ingredient from the Ingredient model
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Get the current user
    user = request.user
    try:
        # Create the new shopping item in the database
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    # Catch the error if its already in there
    except IntegrityError:
        # Don't do anything with the error
        pass
    # Go back to the recipe page
    return redirect("recipe_detail", pk=ingredient.recipe.id)
